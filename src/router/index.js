import Vue from 'vue'
import Router from 'vue-router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'animate.css'
import global from '@/components/global'
import Home from '@/pages/index'
import NewsList from '@/pages/newslist/newslist'
import News from '@/pages/news/news'

Vue.use(Router)
Vue.use(ElementUI)
Vue.prototype.$gloabl = global

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        title: '特惠算首页',
        description: '123'
      }
    },
    {
      path: '/news',
      name: 'News',
      component: News,
      meta: {
        title: ''
      }
    },
    {
      path: '/newslist',
      name: 'NewsList',
      component: NewsList,
      meta: {
        title: '特惠算首页'
      }
    }
  ]
})
